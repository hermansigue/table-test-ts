import React, { useState } from 'react';
import './App.css';
import { RESPONSE_SAMPLE_DATA } from './utils/constant';
import { IDataResponse, IResponseSample } from './models/interface/Dashboard';
import { TableComponent } from './components/TableComponent';
import { Alert, Button, Col, Container, Row } from 'react-bootstrap';
import { ModalEditComponent } from './components/modals/ModalEditComponent';

function App() {
  const [showEditModal, setShowEditModal] = useState(false);
  const [currentData, setCurrentData] = useState({});
  const [isSuccess, setIsSuccess] = useState(false);

  const parsingResponseCast: IResponseSample = JSON.parse(RESPONSE_SAMPLE_DATA);
  let [parsingResponse, setParsingResponse] = useState(parsingResponseCast);

  let [currentIndex, setCurrentIndex] = useState(-1);

  const handleClose = () => setShowEditModal(false);
  const handleOpen = () => setShowEditModal(true);
  const setDetailData = (data: IDataResponse | any, index: number) => {
    setCurrentData(data);
    setCurrentIndex(index);
  };
  const setSuccessEdit = (state: boolean) => setIsSuccess(state);

  return (
    <>
      <div style={{ minHeight: '100vh' }} className="border d-flex align-items-center justify-content-center">
        <Container>
          <Row>
            <Col><h3>Testing Table Typescript</h3></Col>
          </Row>
          <Row hidden={!isSuccess}>
            <Col>
              <Alert key={'success'} variant={'success'}>
                Data edited successfully.
              </Alert>
            </Col>
          </Row>
          <Row>
            <Col>
              <TableComponent
                data={parsingResponse.data}
                graph={parsingResponse.graph}
                id={'list'}
                setDetailData={setDetailData}
                onOpenDetailData={handleOpen}
              />
            </Col>
          </Row>
        </Container>
      </div>
      <ModalEditComponent
        index={currentIndex}
        allData={parsingResponse.data}
        data={currentData}
        graph={parsingResponse.graph}
        isShow={showEditModal}
        onCloseEdit={handleClose}
        callBackSuccess={setSuccessEdit}
      />
    </>
  );
}

export default App;
