import moment from 'moment';
import { PROP_TYPE_DATE, PROP_TYPE_STRING } from '../constant';

export const dateNow = (date?: moment.MomentInput, format?: string) => (date ? moment(date, format) : moment());

export const handlingDataType = function (dataTypeInfo: string, value: any) {

  let result = '';
  switch (dataTypeInfo) {
    case PROP_TYPE_DATE:
      result = dateNow(value).format('YYYY-MM-DD H:m:s')
      break;

    case PROP_TYPE_STRING:
      result = value;
      break;

    default:
      result = value;
      break;
  }
  return result;
};

export function isEmpty(data: any = null): boolean {
  let result = false;
  if (typeof data === 'object') {
    if (JSON.stringify(data) === '{}' || JSON.stringify(data) === '[]') { result = true; }
    if (!data) result = true;
  } else if (typeof data === 'string') {
    if (!data.trim()) result = true;
  } else if (typeof data === 'undefined') {
    result = true;
  }

  return result;
}
