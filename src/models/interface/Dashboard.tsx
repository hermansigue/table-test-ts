interface IPropsResponse {
  type: string;
  value: string;
  name: string;
}

export interface IDataResponse {
  _id: number;
  name: string;
  createdAt: Date;
}

export interface IGraphResponse {
  mamat: string;
  elements: {
    createdAt: {
      props: IPropsResponse;
    },
    name: {
      props: IPropsResponse;
    }
  }
}

export interface IResponseSample {
  title: string;
  data: IDataResponse[];
  graph: IGraphResponse;
}