import { Button, Form, Modal } from "react-bootstrap";
import { IDataResponse, IGraphResponse } from "../../models/interface/Dashboard";
import { dateNow, isEmpty } from "../../utils/helpers";
import Datetime from 'react-datetime';
import { PROP_TYPE_DATE } from "../../utils/constant";

interface Props<T> {
  allData: IDataResponse[];
  data: IDataResponse | any;
  graph: IGraphResponse;
  index: number;
  isShow?: boolean;
  onCloseEdit?: () => void;
  setAllData?: () => void;
  callBackSuccess?: (state: boolean) => void;
}

let formData: any = {
  name: '',
  createdAt: ''
};

let editedIndex: number;
let allCurrentData: IDataResponse[];

function onDataChange(e: any) {
  const key = e.target.name;
  const value = e.target.value;
  formData[key] = value;
}

function onDateChange(key: string, value: string) {
  formData[key] = value;
}

function saveDetail() {
  allCurrentData[editedIndex] = { ...allCurrentData[editedIndex], ...formData };
}

export function ModalEditComponent<T>({
  allData, data, graph, index, isShow, onCloseEdit, callBackSuccess,
}: Props<T>): JSX.Element {
  const keys = Object.keys(graph?.elements);
  const elementsTemp: any = graph?.elements;
  allCurrentData = allData;
  editedIndex = index;
  return (
    <>
      <Modal show={isShow}>
        <Modal.Header>
          <Modal.Title>Edit Dashboard</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            {keys.map((col, colIndex) => {
              formData[col] = data[col];

              switch (elementsTemp[col]?.props?.type) {
                case PROP_TYPE_DATE:
                  return (
                    <Form.Group className="mb-3" key={`row-col-${colIndex}`}>
                      <Form.Label>{elementsTemp[col]?.props?.name}</Form.Label>
                      <Datetime
                        dateFormat="YYYY-MM-DD"
                        timeFormat="HH:mm:ss"
                        onChange={(e) => {
                          onDateChange(col, dateNow(e).format('YYYY-MM-DD HH:mm:ss'));
                        }}
                        initialValue={dateNow(!isEmpty(data[col]) ? data[col] : '-').format('YYYY-MM-DD HH:mm:ss')}
                        inputProps={{
                          placeholder: `Enter ${elementsTemp[col]?.props?.name}`,
                          name: col
                        }}
                      />
                    </Form.Group>
                  )
                  break;

                default:
                  return (
                    <Form.Group className="mb-3" key={`row-col-${colIndex}`} >
                      <Form.Label>{elementsTemp[col]?.props?.name}</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder={`Enter ${elementsTemp[col]?.props?.name}`}
                        name={col}
                        onChange={onDataChange}
                        defaultValue={!isEmpty(data[col]) ? data[col] : '-'}
                      />
                    </Form.Group>
                  );
                  break;
              }

            })}

          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onCloseEdit}>Close</Button>
          <Button variant="primary" onClick={() => {
            saveDetail();
            if (onCloseEdit) onCloseEdit();
            if (callBackSuccess) {
              callBackSuccess(true);
              setTimeout(() => { callBackSuccess(false); }, 5000);
            };
          }} >Save</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}