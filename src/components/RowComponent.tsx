import { IDataResponse, IGraphResponse } from "../models/interface/Dashboard";
import { dateNow, handlingDataType, isEmpty } from "../utils/helpers";

interface Props<T> {
  data: IDataResponse[];
  graph: IGraphResponse;
  onOpenDetailData?: () => void;
  setDetailData?: (data: IDataResponse | any, index: number) => void;
}

export function RowComponent<T>({ data, graph, onOpenDetailData, setDetailData }: Props<T>): JSX.Element {
  const keys = Object.keys(graph?.elements);
  const elementsTemp: any = graph?.elements;

  return (
    <>
      {data.map((item, index) => {
        return (
          <tr key={`${index}-row`}>
            <td key={`${index}-row-number`} className="text-center">
              {index + 1}
            </td>
            {keys.map((col, colIndex) => {
              const itemTemp: any = item;
              if (!isEmpty(itemTemp[col])) {
                return <td key={`${index}-row-col-${colIndex}`}> {handlingDataType(elementsTemp[col].props.type ?? null, itemTemp[col])} </td>;
              }
              return <td key={`${index}-row-col-${colIndex}`}>'-'</td>;
            })}
            <td key={`${index}-row-edit`} className="text-center">
              <button onClick={() => {
                if (setDetailData) setDetailData(item, index);
                if (onOpenDetailData) onOpenDetailData();
              }}>Edit</button>
            </td>
          </tr>
        )

      })}
    </>
  );
}
