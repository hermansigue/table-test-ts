import { IGraphResponse } from "../models/interface/Dashboard"

interface Props<T> {
  graph: IGraphResponse;
}

export function HeadComponent<T>({ graph }: Props<T>): JSX.Element {
  return (
    <tr>
      <>
        <th key={'number-header'} className="text-center">No</th>
        {Object.entries(graph?.elements).map(([k, value]) => (
          <th key={`${value?.props?.name}-header`}>{value?.props?.name}</th>
        ))}
        <th key={'edit-header'} className="text-center">Actions</th>
      </>
    </tr>
  )
}