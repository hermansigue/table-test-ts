import { IDataResponse, IGraphResponse } from "../models/interface/Dashboard";
import { HeadComponent } from "./HeadComponent";
import { RowComponent } from "./RowComponent";
import CSS from 'csstype';

interface Props<T> {
  data: IDataResponse[];
  graph: IGraphResponse;
  id?: string;
  onOpenDetailData?: () => void;
  setDetailData?: (data: IDataResponse | any, index: number) => void;
}

const styles: CSS.Properties = { borderCollapse: "collapse" };

export function TableComponent<T>({ data, graph, id, onOpenDetailData, setDetailData }: Props<T>): JSX.Element {
  return (
    <>
      <table id={id} className="table table-bordered">
        <thead>
          <HeadComponent graph={graph} />
        </thead>
        <tbody>
          <RowComponent
            data={data}
            graph={graph}
            setDetailData={setDetailData}
            onOpenDetailData={onOpenDetailData} />
        </tbody>
      </table>
    </>
  );
}