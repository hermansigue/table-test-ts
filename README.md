# Getting Started with Create React App

This project is intended for the Schutze Web trial task. This project is built from a React JS framework that focuses on the front end

## Build with

* [React.js](https://reactjs.org/) ( 18.2.0 =>)
* [Typescript](https://www.npmjs.com/package/typescript) ( 4.9.5 => )
* [React Bootstrap](https://react-bootstrap.github.io/) ( 2.7.2 =>)
* [Moment Js](https://momentjs.com/) ( 2.29.1 =>)
* [Bootstrap](https://getbootstrap.com/) ( 5.2.3 )

## Getting Started

This is an instructions on setting up your project locally.
To get a local copy up and running follow these simple steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* npm
  ```sh
  npm install npm@latest -g
  ```
* npx
  ```sh
  npm install -g npx
  ```
  
* typescript 
  ```sh
  npm install -g typescript
  ```


### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/hermansigue/table-test-ts.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Run Project
   ```sh
   npm start
   ```